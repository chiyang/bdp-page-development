FROM codercom/code-server:4.0.2
USER root
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs
RUN npm install -g npm@8.5.2 && \
    npm install -g @vue/cli @angular/cli
USER coder

RUN code-server --install-extension wayou.vscode-todo-highlight && \
    code-server --install-extension jasonnutter.search-node-modules && \
    code-server --install-extension octref.vetur && \
    code-server --install-extension liuji-jim.vue && \
    code-server --install-extension hollowtree.vue-snippets && \
    code-server --install-extension eg2.vscode-npm-script && \
    code-server --install-extension christian-kohler.npm-intellisense && \
    code-server --install-extension christian-kohler.path-intellisense && \
    code-server --install-extension mgmcdermott.vscode-language-babel && \
    code-server --install-extension hookyqr.beautify && \
    code-server --install-extension ms-ceintl.vscode-language-pack-zh-hant && \
    code-server --install-extension dbaeumer.vscode-eslint && \
    code-server --install-extension oderwat.indent-rainbow && \
    code-server --install-extension xabikos.javascriptsnippets && \
    code-server --install-extension ms-vscode.js-atom-grammar && \
    code-server --install-extension hridoy.jquery-snippets && \
    code-server --install-extension dbaeumer.jshint && \
    code-server --install-extension ritwickdey.live-sass
